package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="articles")
public class Article implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idArticle;
	
	private String codeArticle;
	private String designation;
	private BigDecimal prixUnitaire;
	private BigDecimal tauxTVA;
	private BigDecimal prixUnitaireTTC;
	private String photo;
	
	public Article() {
	}

	@ManyToOne
	@JoinColumn(name="idCategorie")
	private Categorie categorie;

	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}
}
