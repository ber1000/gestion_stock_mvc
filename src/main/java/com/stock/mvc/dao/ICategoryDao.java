package com.stock.mvc.dao;

import com.stock.mvc.entities.Categorie;

public interface ICategoryDao extends IGenericDao<Categorie>{

}
